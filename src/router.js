import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'
import LogIn from './components/LogIn.vue'
import UserDetail from './components/UserDetail.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'

const routes = [
  {
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/users/login',
    name: 'logIn',
    component: LogIn
  },
  {
    path: '/user/details',
    name: 'userDetail',
    component: UserDetail
  },
  {
    path: '/users/signup',
    name: 'signUp',
    component: SignUp
  },
  {
    path: '/home',
    name: 'home',
    component: Home
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
